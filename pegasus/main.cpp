
#include <GLFW/glfw3.h>
#include <nanogui/nanogui.h>

#include <iostream>

using namespace nanogui;

enum class test_enum {
    Item1 = 0,
    Item2,
    Item3
};

bool bvar = true;
int ivar = 12345678;
double dvar = 3.1415926;
float fvar = (float)dvar;
std::string strval = "A string";
std::string strval2 = "";
test_enum enumval = test_enum::Item2;
Color colval(0.5f, 0.5f, 0.7f, 1.f);

class ExampleApplication
    : public Screen
{
public:
    class DecRef
    {
    public:
        void operator()(Object* ptr) noexcept { ptr->dec_ref(); };
    };
    static auto Create() {
        return std::unique_ptr<ExampleApplication, DecRef>(new ExampleApplication());
    }

    ExampleApplication()
        : Screen(
            Vector2i(1920, 1080),
            "NanoGUI Test [GL 4.6]"
        )
    {
        inc_ref();
        m_render_pass = new RenderPass({ this });
        m_render_pass->set_clear_color(0, Color(0.3f, 0.3f, 0.32f, 1.f));

        m_shader = new Shader(
            m_render_pass,

            /* An identifying name */
            "a_simple_shader",

            R"(/* Vertex shader */
            #version 330
            uniform mat4 mvp;
            in vec3 position;
            void main() {
                gl_Position = mvp * vec4(position, 1.0);
            })",

            /* Fragment shader */
            R"(#version 330
            out vec4 color;
            uniform float intensity;
            void main() {
                color = vec4(vec3(intensity), 1.0);
            })"
        );

        uint32_t indices[3 * 2] = {
            0, 1, 2,
            2, 3, 0
        };

        float positions[3 * 4] = {
            -1.f, -1.f, 0.f,
            1.f, -1.f, 0.f,
            1.f, 1.f, 0.f,
            -1.f, 1.f, 0.f
        };

        m_shader->set_buffer("indices", VariableType::UInt32, { 3 * 2 }, indices);
        m_shader->set_buffer("position", VariableType::Float32, { 4, 3 }, positions);
        m_shader->set_uniform("intensity", 0.5f);
    }

    virtual ~ExampleApplication() noexcept = default;

    void draw_contents() override
    {
        Matrix4f mvp = Matrix4f::scale(Vector3f(
            (float)m_size.y() / (float)m_size.x() * 0.25f, 0.25f, 0.25f)) *
            Matrix4f::rotate(Vector3f(0, 0, 1), (float)glfwGetTime());

        m_shader->set_uniform("mvp", mvp);

        if (m_render_pass)
        {
            m_render_pass->resize(framebuffer_size());
            m_render_pass->begin();

            m_shader->begin();
            m_shader->draw_array(Shader::PrimitiveType::Triangle, 0, 6, true);
            m_shader->end();

            m_render_pass->end();
            //Screen::draw_contents();
        }
    }

private:
    ref<RenderPass> m_render_pass;
    ref<Shader> m_shader;
};

int main()
{
    nanogui::init();

    {
        auto screen = ExampleApplication::Create();

        {
            bool enabled = true;
            FormHelper* gui = new FormHelper(screen.get());
            ref<Window> window = gui->add_window(Vector2i(10, 10), "Form helper example");

            gui->add_group("Basic types");
            gui->add_variable("bool", bvar);
            gui->add_variable("string", strval);
            gui->add_variable("placeholder", strval2)->set_placeholder("placeholder");

            gui->add_group("Validating fields");
            gui->add_variable("int", ivar)->set_spinnable(true);
            gui->add_variable("float", fvar);
            gui->add_variable("double", dvar)->set_spinnable(true);

            gui->add_group("Complex types");
            gui->add_variable("Enumeration", enumval, enabled)
                ->set_items({ "Item 1", "Item 2", "Item 3" });
            gui->add_variable("Color", colval);

            gui->add_group("Other widgets");
            gui->add_button("A button", [&]() { screen->set_visible(false); });

            screen->draw_all();
            screen->set_visible(true);
            screen->perform_layout();
            window->center();

            nanogui::mainloop(16);
        }
    }

    nanogui::shutdown();
    return 0;
}
